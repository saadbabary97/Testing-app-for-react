import React from 'react';
import ReactDOM from 'react-dom';
import { StyledEngineProvider } from '@mui/material/styles';
// import Demo from '../src/MyComponents/demo'

ReactDOM.render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      {/* <Demo /> */}
    </StyledEngineProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
