import React, { useEffect, useState } from "react";
import Education from './Education'; 
import Experience from "./Experience";
import Hobby from "./Hobby";
import Skill from "./Skill";
import axios from "axios";
import '../index.css'; 
import '../light_theme.css'; 
import { toast, ToastContainer } from "react-toastify";


const Portfolio = () => {
  const [portfolioData, setPortfolioData] = useState({});
  const [id, setId] = useState(1); 
  const [inputId, setInputId] = useState(""); 
  const token = localStorage.getItem("token");

  const GetResume = (id) => {
    axios
      .get(`http://127.0.0.1:9001/api/test_app/portfolio/${id}`, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        setPortfolioData(res.data);
      })
      .catch((error) => {
      });
    setId(id);
  };

  console.log("portfolioData:........",portfolioData)
  
  const PostResume = () => {
    axios
      .post(`http://127.0.0.1:9001/api/test_app/create_portfolio/`, portfolioData, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        setPortfolioData(res.data);
      })
      .catch((error) => {
      });
  };
  

  useEffect(() => {
    GetResume(id);
  }, [id]); 

  const handleIdChange = (event) => {
    setInputId(event.target.value);
  };

  const handleIdSubmit = () => {
    if (inputId) {
      setId(parseInt(inputId));
    }
  };

  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setPortfolioData((portfolioData) => ({
    ...portfolioData,
    [name]: value,
    }));
  };



  return (
    <div className="portfolio-container light-theme">
      <ToastContainer />
      <div className="portfolio-content">
        <div className="header">
          <h1>Resume</h1>
        </div>
        <div className="id-input">
          <input
            type="text"
            placeholder="Enter ID"
            value={inputId}
            onChange={handleIdChange}
          />
          <button onClick={handleIdSubmit}>Generate Data Against User Id.</button>
        </div>
        <div className="personal-info">
          <div className="row">
            <div className="col-md-6">
              <div className="info-label">User Name:</div>
              <input 
                className="form-control"
                name="user_name"
                type="text"
                value={portfolioData.user_name || ''}
                onChange={(e) => handleFieldChange(e)} 
                />
            </div>
            <div className="col-md-6">
              <div className="info-label">Full Name:</div>
              <input 
                    className="form-control"
                    name="full_name" 
                    type="text" 
                    value={portfolioData.full_name || ''}  
                    onChange={(e) => handleFieldChange(e)} 
                    />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="info-label">Address:</div>
              <textarea
                    className="form-control" 
                    name="address" 
                    value={portfolioData.address || ''} 
                    onChange={(e) => handleFieldChange(e)} 
                    />
            </div>
            <div className="col-md-6">
              <div className="info-label">Phone Number:</div>
              <input 
                    className="form-control" 
                    name="phone_number" 
                    type="number" 
                    value={portfolioData.phone_number || ''} 
                    onChange={(e) => handleFieldChange(e)} 
                    />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="info-label">Email:</div>
              <input 
                    className="form-control" 
                    name="email" 
                    type="text" 
                    value={portfolioData.email || ''} 
                    onChange={(e) => handleFieldChange(e)} 
                    />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="info-label">Additional Information:</div>
              <textarea 
                    className="form-control" 
                    name="additional_information" 
                    value={portfolioData.additional_information || ''} 
                    onChange={(e) => handleFieldChange(e)} 
                    />
            </div>
          </div>
        </div>
        <div className="sections">
          <div className="section">
            <h2>Education:</h2>
            <Education 
              id={portfolioData.id}
              inputId={inputId}
              setInputId={setInputId} 
              GetResume={GetResume}
              portfolioData={portfolioData}
              setPortfolioData={setPortfolioData}
            />
          </div>
          <div className="section">
            <h2>Experience:</h2>
            <Experience 
              id={portfolioData.id}
              inputId={inputId}
              GetResume={GetResume}
              portfolioData={portfolioData}
              setPortfolioData={setPortfolioData}
              />
          </div>
          <div className="section">
            <h2>Hobby:</h2>
            <Hobby 
              id={portfolioData.id}
              inputId={inputId}
              GetResume={GetResume}
              portfolioData={portfolioData}
              setPortfolioData={setPortfolioData}
            />
          </div>
          <div className="section">
            <h2>Skill:</h2>
            <Skill 
              id={portfolioData.id}
              inputId={inputId}
              GetResume={GetResume}
              portfolioData={portfolioData}
              setPortfolioData={setPortfolioData}
              />
          </div>
        </div>
      </div>
      <div className="col-md-9 text-right">
            <button 
                type="submit" 
                className='btn btn-primary'
                onClick={() => PostResume()}
                > Save
            </button>
        </div>
    </div>
  );
};

export default Portfolio;
