import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const EditHobby = ({formData, setFormData, setModalEdit, EditHobbyData, hobbyId}) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalEdit} onHide={() => setModalEdit(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Hobby
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
              <div className="col-md-12">
                <label>Hobby name:</label>
                <input
                  type="text"
                  name="hobby_name"
                  className="form-control"
                  value={formData?.hobby_name}
                  onChange={handleFieldChange}
                />
              </div>
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalEdit(false)}>
          Cancel
        </Button>
        <Button variant="primary" 
        onClick={() => EditHobbyData(hobbyId)}
        >
         Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditHobby;
