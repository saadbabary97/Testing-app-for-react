import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const AddHobby = ({formData, setFormData, CreateHobby, setModalShow }) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalShow} onHide={() => setModalShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Hobby
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
                  <label>Hobby Name: </label>
                  <input
                    type="text"
                    name="hobby_name"
                    className="form-control"
                    value={formData?.hobby_name}
                    onChange={handleFieldChange}
                  />
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalShow(false)}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => CreateHobby()}>
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddHobby;
