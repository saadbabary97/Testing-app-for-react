import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const AddSkill = ({formData, setFormData, CreateSkill, setModalShow }) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalShow} onHide={() => setModalShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Skill
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
                  <label>Skill</label>
                  <input
                    type="text"
                    name="skill_name"
                    className="form-control"
                    value={formData?.skill_name}
                    onChange={handleFieldChange}
                  />
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalShow(false)}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => CreateSkill()}>
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddSkill;
