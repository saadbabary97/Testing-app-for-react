import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const AddExperience = ({formData, setFormData, CreateExperience, setModalShow }) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalShow} onHide={() => setModalShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Experience
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
              <div className="col-md-12">
                <label>Company name:</label>
                <input
                  type="text"
                  name="company_name"
                  className="form-control"
                  value={formData?.company_name}
                  onChange={handleFieldChange}
                />
              </div>
              <div className="col-md-12">
                <label>Experience:</label>
                <input
                  type="number"
                  name="experience"
                  className="form-control"
                  value={formData?.experience}
                  onChange={handleFieldChange}
                  min={0}
                />
              </div>
              <div className="col-md-12">
                <label>Position:</label>
                <input
                  type="text"
                  name="position"
                  className="form-control"
                  value={formData?.position}
                  onChange={handleFieldChange}
                />
              </div>
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalShow(false)}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => CreateExperience()}>
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddExperience;
