import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const EditSkill = ({formData, setFormData, setModalEdit, EditSkillData, skillId}) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalEdit} onHide={() => setModalEdit(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Skill
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
              <div className="col-md-12">
                <label>Skill name:</label>
                <input
                  type="text"
                  name="skill_name"
                  className="form-control"
                  value={formData?.skill_name}
                  onChange={handleFieldChange}
                />
              </div>
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalEdit(false)}>
          Cancel
        </Button>
        <Button variant="primary" 
        onClick={() => EditSkillData(skillId)}
        >
         Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditSkill;
