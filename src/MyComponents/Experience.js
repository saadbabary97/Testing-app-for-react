import React, { useEffect, useState } from "react";
import { Card } from "@mui/material";
import axios from "axios";
import AddExperience from "./Pop Up/AddExperience";
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteOutlineTwoToneIcon from '@mui/icons-material/DeleteOutlineTwoTone';
import EditExperience from "./Pop Up/EditExperience";

const Experience = ({ id, GetResume, portfolioData, setPortfolioData}) => {
  const token = localStorage.getItem("token");
  const [modalShow, setModalShow] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [editingExperience, setEditingExperience] = useState(null);


  const [formData, setFormData] = useState({
    company_name: "",
    experience: "",
    position: "",
  });

  useEffect(() => {
    setFormData({
      company_name: "",
      experience: "",
      position: "",
    });
  }, [portfolioData]);
  
  const CreateExperience = () => {
    const userIdentifier = portfolioData.user_name;
    const postExperience = {
      ...formData,
      username: userIdentifier,
    };
  
    axios
      .post(`http://127.0.0.1:9001/api/test_app/create_experience`, postExperience, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        const newExperience = res.data;
        setFormData({ company_name: "", experience: "", position: "",});
        setModalShow(false);
        setModalEdit(false)
        setPortfolioData((prevData) => ({
          ...prevData,
          work_experiences: [...prevData.work_experiences, newExperience],
        }));
      })
      .catch(() => {
      });
  };
  
  const EditExperienceData = () => {
    if (!editingExperience){
      return;
    }
    const experienceId = editingExperience.id
    const userIdentifier = portfolioData.user_name;
    const postData = {
      id: experienceId,
      username: userIdentifier,
      ...formData,
    };
  
    axios
      .patch(`http://127.0.0.1:9001/api/test_app/update_experience/${experienceId}/`, postData, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {

        setPortfolioData((prevData) => ({
          ...prevData,
          work_experiences: prevData.work_experiences.map((experience) =>
            experience.id === experienceId ? { ...experience, 
              company_name:res.data.company_name,
              experience:res.data.experience,
              position: res.data.position }
              : experience
          ),
        }));
        setModalShow(false);
        setModalEdit(false);
        setEditingExperience(null)

        setPortfolioData((prevData) => ({
          ...prevData,
          work_experiences: prevData.work_experiences.map((experience) =>
            experience.id === experienceId ? res.data : experience
          ),
        }));
      })
      .catch((error) => {
        console.error('Error updating experience:', error);
      });
  };
  

const DeleteExperience = (experienceIdToDelete) => {
  const userIdentifier = portfolioData.user_name;

  axios
    .delete(`http://127.0.0.1:9001/api/test_app/delete_experience/${experienceIdToDelete}/`, userIdentifier, {
      headers: {
        Authorization: `Token ${token}`,
      },
    })
    .then(() => {
      const updatedExperience = portfolioData.work_experiences.filter(
        (work_experiences) => work_experiences.id !== experienceIdToDelete
      );
      setPortfolioData((prevData) => ({
        ...prevData,
        work_experiences: updatedExperience,
      }));
    })
    .catch((error) => {
      console.error('Error deleting experience', error);
    });
};

  const handleModal = () => {
    setModalShow(true);
    setFormData({
      company_name: "",
      experience: "",
      position: "",
    });
  };

  const handleModalEdit = (experienceToEdit) => {
    setFormData({
      company_name: experienceToEdit.company_name,
      experience: experienceToEdit.experience,
      position: experienceToEdit.position,
    });
    setEditingExperience(experienceToEdit)
    setModalEdit(true);
    setModalShow(false);
  };
  
  useEffect(() => {
    GetResume(id);
  }, [id]);

  return (
    <div className="container-fluid">
      <div className="education-data">
        {portfolioData?.work_experiences?.map((experience, index) => (
          <Card className="col-md-12" key={index}>
            <div className="container">
              <div className="row my-4">
                <div className="col-md-12">
                  <label>Company name:</label>
                  <input
                    type="text"
                    name={`company_name${index}`}
                    className="form-control"
                    value={experience.company_name}
                  />
                </div>
                <div className="col-md-12">
                  <label>Experience:</label>
                  <input
                    type="number"
                    name={`experience${index}`}
                    className="form-control"
                    value={experience.experience}
                  />
                </div>
                <div className="col-md-12">
                  <label>Position:</label>
                  <input
                    type="text"
                    name={`position${index}`}
                    className="form-control"
                    value={experience.position}
                  />
                </div>
              </div>
              {modalShow && (
                <AddExperience
                  formData={formData}
                  setFormData={setFormData}
                  setModalShow={setModalShow}
                  CreateExperience={CreateExperience}
                />
                )}
                {modalEdit && (
                  <EditExperience
                    formData={formData}
                    setFormData={setFormData}
                    setModalEdit={setModalEdit}
                    EditExperienceData={EditExperienceData}
                    experienceId={experience.id}

                  />
                )}
              <div className="col-md-12 mt-2 text-right">
                {index === portfolioData.work_experiences.length - 1 && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={handleModal}
                  >
                    <AddTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.work_experiences.length && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={()=>handleModalEdit(experience)}
                  >
                    <EditTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.work_experiences.length - 1 && (
                    <a
                      type="button"
                      className="btn btn-primary mx-2 my-2"
                      onClick={() => DeleteExperience(experience.id)}
                    >
                      <DeleteOutlineTwoToneIcon />
                    </a>
                  )}
              </div>
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Experience;
