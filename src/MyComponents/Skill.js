import React, { useEffect, useState } from "react";
import { Card } from "@mui/material";
import axios from "axios";
import AddSkill from "./Pop Up/AddSkill";
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteOutlineTwoToneIcon from '@mui/icons-material/DeleteOutlineTwoTone';
import EditSkill from "./Pop Up/EditSkill";

const Skill = ({ id, GetResume, portfolioData, setPortfolioData }) => {
  const [formData, setFormData] = useState({
    skill_name: "",
  });
  const [errors, setErrors] = useState({});
  const token = localStorage.getItem("token");

  const [modalEdit, setModalEdit] = useState(false);
  const [modalShow, setModalShow] = useState(false);

  const [editingSkill, setEditingSkill] = useState(null);


  const CreateSkill = (id) => {
    const userIdentifier = portfolioData.user_name;
    const formDataWithUser = {
      ...formData,
      username: userIdentifier,
    };
  
    axios
      .post(`http://127.0.0.1:9001/api/test_app/create_skill`, formDataWithUser, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        const newSkill = res.data;
        setFormData({ skill_name: "" });
        setModalShow(false);
        setPortfolioData((prevData) => ({
          ...prevData,
          skills: [...prevData.skills, newSkill],
        }));
      })
      .catch(() => {
      });
  };


  const EditSkillData = () => {
    if (!editingSkill) {
      return;
    }
  
    const skillId = editingSkill.id;
    const userIdentifier = portfolioData.user_name;
    const postData = {
      id: skillId,
      username: userIdentifier,
      skill_name: formData.skill_name,
    };
  
    axios
      .patch(`http://127.0.0.1:9001/api/test_app/update_skill/${skillId}/`, postData, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        setPortfolioData((prevData) => ({
          ...prevData,
          skills: prevData.skills.map((skill) =>
            skill.id === skillId ? { ...skill, skill_name: res.data.skill_name } : skill
          ),
        }));
  
        setModalShow(false);
        setModalEdit(false);
        setEditingSkill(null);
      })
      .catch((error) => {
        console.error('Error updating skill:', error);
      });
  };

  const DeleteSkill = (skillIdToDelete) => {
    const userIdentifier = portfolioData.user_name;
  
    axios
      .delete(`http://127.0.0.1:9001/api/test_app/delete_skill/${skillIdToDelete}/`, userIdentifier, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then(() => {
        console.log('Skill deleted successfully');
        const updatedSkill = portfolioData.skills.filter(
          (skill) => skill.id !== skillIdToDelete
        );
        setPortfolioData((prevData) => ({
          ...prevData,
          skills: updatedSkill,
        }));
      })
      .catch((error) => {
        console.error('Error deleting skill', error);
      });
  };
  

const handleModal = () => {
  setModalShow(true);
  setFormData({
    skill_name:"",
  });
};

const handleModalEdit = (skillToEdit) => {
  setFormData({
    skill_name: skillToEdit.skill_name,
  });
  setEditingSkill(skillToEdit);
  setModalEdit(true);
  setModalShow(false);
};



  useEffect(() => {
    GetResume(id);
  }, [id]);

  return (
    <div className="container-fluid">
      <div className="hobbies-data">
        {portfolioData?.skills?.map((skill, index) => (
          <Card className="col-md-12" key={index}>
            <div className="container">
              <div className="row my-4">
                <div className="col-md-12">
                  <label>{`Skill ${index + 1}:`}</label>
                  <input
                    type="text"
                    name={`skill${index}`}
                    className="form-control"
                    value={skill.skill_name}
                    onChange={(e) => {
                      setFormData({ ...formData, skill_name: e.target.value });
                      setErrors({ ...errors, missingFields: false });
                    }}
                  />
                </div>
                {modalShow && (
                <AddSkill
                  formData={formData}
                  setFormData={setFormData}
                  setModalShow={setModalShow}
                  CreateSkill={CreateSkill}
                />
                )}
                {modalEdit && (
                  <EditSkill
                    formData={formData}
                    setFormData={setFormData}
                    setModalEdit={setModalEdit}
                    EditSkillData={EditSkillData}
                    skillId={skill.id}

                  />
                )}
              </div>
              <div className="col-md-12 mt-2 text-right">
                {index === portfolioData.skills.length - 1 && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={handleModal}
                  >
                    <AddTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.skills.length  && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={()=>handleModalEdit(skill)}
                  >
                    <EditTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.skills.length - 1 && (
                    <a
                      type="button"
                      className="btn btn-primary mx-2 my-2"
                      onClick={() => DeleteSkill(skill.id)}
                    >
                      <DeleteOutlineTwoToneIcon />
                    </a>
                  )}
              </div>
              </div>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Skill;



