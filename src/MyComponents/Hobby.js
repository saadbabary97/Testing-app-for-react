import React, { useEffect, useState } from "react";
import { Card } from "@mui/material";
import AddHobby from "../MyComponents/Pop Up/AddHobby"
import axios from "axios";
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteOutlineTwoToneIcon from '@mui/icons-material/DeleteOutlineTwoTone';
import EditHobby from "./Pop Up/EditHobby";


const Hobby = ({ id, GetResume, portfolioData, setPortfolioData }) => {
  const token = localStorage.getItem("token");
  const [modalShow, setModalShow] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);

  const [formData, setFormData] = useState({
    hobby_name: "",
  });
  const[editingHobby, setEditingHobby] = useState(null);

  const CreateHobby = () => {
    const userIdentifier = portfolioData.user_name;
    const postHobby = {
      ...formData,
      username: userIdentifier,
    };
  
    axios
      .post(`http://127.0.0.1:9001/api/test_app/create_hobby`, postHobby, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        const newHobby = res.data;
        setFormData({ hobby_name: ""});
        setModalShow(false);
        setPortfolioData((prevData) => ({
          ...prevData,
          hobbies: [...prevData.hobbies, newHobby],
        }));
      })
      .catch(() => {
      });
  };

  const EditHobbyData = () => {
    if (!editingHobby) {
      return;
    }
  
    const hobbyId = editingHobby.id;
    const userIdentifier = portfolioData.user_name;
    const postData = {
      id: hobbyId,
      username: userIdentifier,
      hobby_name: formData.hobby_name,
    };
  
    axios
      .patch(`http://127.0.0.1:9001/api/test_app/update_hobby/${hobbyId}/`, postData, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((res) => {
        setPortfolioData((prevData) => ({
          ...prevData,
          hobbies: prevData.hobbies.map((hobby) =>
            hobby.id === hobbyId ? { ...hobby, hobby_name: res.data.hobby_name } : hobby
          ),
        }));
  
        setModalShow(false);
        setModalEdit(false);
        setEditingHobby(null);
      })
      .catch((error) => {
        console.error('Error updating hobby:', error);
      });
  };


  const DeleteHobby = (hobbyIdToDelete) => {
    const userIdentifier = portfolioData.user_name;
  
    axios
      .delete(`http://127.0.0.1:9001/api/test_app/delete_hobby/${hobbyIdToDelete}/`, userIdentifier, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then(() => {
        console.log('Hobby deleted successfully');
        const updatedHobby = portfolioData.hobbies.filter(
          (hobby) => hobby.id !== hobbyIdToDelete
        );
        setPortfolioData((prevData) => ({
          ...prevData,
          hobbies: updatedHobby,
        }));
      })
      .catch((error) => {
        console.error('Error deleting education', error);
      });
  };
  
  
  const handleModal = () => {
    setModalShow(true);
    setFormData({
      hobby_name:"",
    })
  };
  
  const handleModalEdit = (hobbyToEdit) => {
    setFormData({
      hobby_name: hobbyToEdit.hobby_name,
    });
    setEditingHobby(hobbyToEdit);
    setModalEdit(true);
    setModalShow(false);
  };

  useEffect(() => {
    GetResume(id);
  }, [id]);

  return (
    <div className="container-fluid">
      <div className="hobbies-data">
        {portfolioData?.hobbies?.map((hobby, index) => (
          <Card className="col-md-12" key={index}>
            <div className="container">
              <div className="row my-4">
                <div className="col-md-12">
                  <label>{`Hobby ${index + 1}:`}</label>
                  <input
                    type="text"
                    name={`hobby${index}`}
                    className="form-control"
                    value={hobby.hobby_name}
                  />
                </div>
              </div>
              {modalShow && (
                <AddHobby
                  formData={formData}
                  setFormData={setFormData}
                  setModalShow={setModalShow}
                  CreateHobby={CreateHobby}
                />
                )}
              {modalEdit && (
                  <EditHobby
                    formData={formData}
                    setFormData={setFormData}
                    setModalEdit={setModalEdit}
                    EditHobbyData={EditHobbyData}
                    hobbyId={hobby.id}

                  />
                )}
              <div className="col-md-12 mt-2 text-right">
                {index === portfolioData.hobbies.length - 1 && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={handleModal}
                  >
                    <AddTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.hobbies.length && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={()=>handleModalEdit(hobby)}
                  >
                    <EditTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.hobbies.length - 1 && (
                    <a
                      type="button"
                      className="btn btn-primary mx-2 my-2"
                      onClick={() => DeleteHobby(hobby.id)}
                    >
                      <DeleteOutlineTwoToneIcon />
                    </a>
                  )}
              </div>
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Hobby;
