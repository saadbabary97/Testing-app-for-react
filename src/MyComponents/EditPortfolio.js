import React, { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Card } from "@mui/material";

const EditPortfolio = ({formData, setFormData, EditEducation, setModalEdit, educationId}) => {
  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };
  return (
    <Modal size="md" show={setModalEdit} onHide={() => setModalEdit(false)}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Education
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Card className="col-md-12">
          <div className="container">
            <div className="row my-4">
              <div className="col-md-12">
                <label>School name:</label>
                <input
                  type="text"
                  name="school_name"
                  className="form-control"
                  value={formData?.school_name}
                  onChange={handleFieldChange}
                />
              </div>
              <div className="col-md-12">
                <label>College name:</label>
                <input
                  type="text"
                  name="college_name"
                  className="form-control"
                  value={formData?.college_name}
                  onChange={handleFieldChange}
                />
              </div>
              <div className="col-md-12">
                <label>Degree:</label>
                <input
                  type="text"
                  name="degree"
                  className="form-control"
                  value={formData?.degree}
                  onChange={handleFieldChange}
                />
              </div>
            </div>
          </div>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setModalEdit(false)}>
          Cancel
        </Button>
        <Button variant="primary" 
        onClick={() => EditEducation(educationId)}
        >
         Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditPortfolio;
