import React, { useEffect, useState } from "react";
import { Card } from "@mui/material";
import AddPortfolio from "./AddPortfolio";
import EditPortfolio from "./EditPortfolio";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteOutlineTwoToneIcon from '@mui/icons-material/DeleteOutlineTwoTone';

const Education = ({ id, GetResume, portfolioData, setPortfolioData}) => {
  const [formData, setFormData] = useState({
    school_name: "",
    college_name: "",
    degree: "",
  });

  const [modalShow, setModalShow] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const token = localStorage.getItem("token");
  const [editingEducation, setEditingEducation] = useState(null);


  useEffect(() => {
    setFormData({
      school_name: "",
      college_name: "",
      degree: "",
    });
  }, [portfolioData]);
  
  const CreateEducation = () => {
    const userIdentifier = portfolioData.user_name;
    const postEducation = {
      ...formData,
      username: userIdentifier,
    };

    axios
    .post(`http://127.0.0.1:9001/api/test_app/create_education`, postEducation, {
      headers: {
          Authorization: `Token ${token}`,
        },
      })
    .then((res) => {
      const newEducation = res.data;
      setFormData({ school_name: "", college_name: "", degree: "", });
      setModalShow(false);
      setModalEdit(false)
      setPortfolioData((prevData) => ({
        ...prevData,
        educations: [...prevData.educations, newEducation],
      }));
    })
    .catch(() => {
    });
};

const EditEducation = () => {
  if (!editingEducation) {
    return;
  }
  const educationId = editingEducation.id
  const userIdentifier = portfolioData.user_name;
  const postData = {
    id: educationId,
    username: userIdentifier,
    ...formData,
  };

  axios
    .patch(`http://127.0.0.1:9001/api/test_app/update_education/${educationId}/`, postData, {
      headers: {
        Authorization: `Token ${token}`,
      },
    })
    .then((res) => {
      setPortfolioData((prevData) => ({
        ...prevData,
        educations: prevData.educations.map((education) =>
          education.id === educationId ? { ...education, 
            school_name:res.data.school_name,
            college_name:res.data.college_name,
            degree: res.data.degree }
            : education
        ),
      }));

      setModalShow(false);
      setModalEdit(false);
      setEditingEducation(null)
     
    })
    .catch((error) => {
      console.error('Error updating education:', error);
    });
};


const DeleteEducation = (educationIdToDelete) => {
  const userIdentifier = portfolioData.user_name;

  axios
    .delete(`http://127.0.0.1:9001/api/test_app/delete_education/${educationIdToDelete}/`, userIdentifier, {
      headers: {
        Authorization: `Token ${token}`,
      },
    })
    .then(() => {
      const updatedEducations = portfolioData.educations.filter(
        (education) => education.id !== educationIdToDelete
      );
      setPortfolioData((prevData) => ({
        ...prevData,
        educations: updatedEducations,
      }));
    })
    .catch((error) => {
      console.error('Error deleting education', error);
    });
};

  const handleModal = () => {
    setModalShow(true);
    setFormData({
      school_name: "",
      college_name: "",
      degree: "",
    });

  };

  const handleModalEdit = (educationToEdit) => {
    setFormData({
      school_name: educationToEdit.school_name,
      college_name: educationToEdit.college_name,
      degree: educationToEdit.degree,
    });
    setEditingEducation(educationToEdit);
    setModalEdit(true);
    setModalShow(false);
  };

  useEffect(() => {
    GetResume(id);
  }, [id]);

  return (
    <div className="container-fluid">
      <div className="education-data">
      <ToastContainer />
        {portfolioData?.educations?.map((education, index) => (
          <Card className="col-md-12" key={index}>
            <div className="container">
              <div className="row my-4">
                <div className="col-md-12">
                  <label>School name:</label>
                  <input
                    type="text"
                    name={`school_name_${index}`}
                    className="form-control"
                    value={education.school_name}
                  />
                </div>
                <div className="col-md-12">
                  <label>College name:</label>
                  <input
                    type="text"
                    name={`college_name_${index}`}
                    className="form-control"
                    value={education.college_name}
                  />
                </div>
                <div className="col-md-12">
                  <label>Degree:</label>
                  <input
                    type="text"
                    name={`degree_${index}`}
                    className="form-control"
                    value={education.degree}
                  />
                </div>
                {modalShow && (
                  <AddPortfolio
                    formData={formData}
                    setFormData={setFormData}
                    setModalShow={setModalShow}
                    CreateEducation={CreateEducation}
                  />
                )}
                {modalEdit && (
                  <EditPortfolio
                    formData={formData}
                    setFormData={setFormData}
                    setModalEdit={setModalEdit}
                    EditEducation={EditEducation}
                    educationId={education.id}

                  />
                )}
                <div className="col-md-12 mt-2 text-right">
                {index === portfolioData.educations.length - 1 && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={handleModal}
                  >
                    <AddTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.educations.length && (
                  <a
                    type="button"
                    className="btn btn-primary mx-2 my-2"
                    onClick={() => handleModalEdit(education)}
                  >
                    <EditTwoToneIcon />
                  </a>
                )}
                {index !== portfolioData.educations.length - 1 && (
                    <a
                      type="button"
                      className="btn btn-primary mx-2 my-2"
                      onClick={() => DeleteEducation(education.id)}
                    >
                      <DeleteOutlineTwoToneIcon />
                    </a>
                  )}
              </div>
              </div>
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Education;




