import './App.css';
import Header from './MyComponents/Header';
import Footer from './MyComponents/Footer';
import Portfolio from './MyComponents/Portfolio';
import Education from './MyComponents/Education';
import Hobby from './MyComponents/Hobby';
import Skill from './MyComponents/Skill';
import Experience from './MyComponents/Experience';
import React from 'react';
import { Routes, Route } from "react-router-dom"
import Login from './MyComponents/Login';

function App() {
  return (
    <>
      <Header title='' searchbar={true} />
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/portfolio" element={ <Portfolio  />} />
        <Route path="/education" element={ <Education />} />
        <Route path="/hobby" element={ <Hobby />} />
        <Route path="/skill" element={ <Skill />} />
        <Route path="/experience" element={ <Experience />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
